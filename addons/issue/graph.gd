@tool
extends MarginContainer


## I can't find any suggestions about how to use force_drag, so I cooked this up
## However it crashes on drop.


var dummyPreview : ColorRect
var _draggo:=false

func _process(delta: float) -> void:
	if _draggo:
		force_drag("Some data", dummyPreview)
	else:
		if is_instance_valid(dummyPreview):
			if not dummyPreview.is_queued_for_deletion():
				dummyPreview.queue_free()

func _on_panel_container_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if event.is_pressed():
			if event.button_index == 1:
				if not _draggo:
					print("drag?")
					dummyPreview = ColorRect.new()
					dummyPreview.size = Vector2(50,50)
					dummyPreview.color = Color(1,0,0,1)
					_draggo = true
		else:
			print("drop?")
			_draggo = false
