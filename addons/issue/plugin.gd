@tool
extends EditorPlugin

const GraphScene := preload("graph.tscn")
const Graph := preload("graph.gd")

var graph_view: Graph
var rwbutton

func _enter_tree() -> void:
	if Engine.is_editor_hint():
		graph_view = GraphScene.instantiate()
		## get_editor_interface().get_editor_main_screen().add_child(main_view)
		rwbutton = add_control_to_bottom_panel(graph_view, "Issue")


func _exit_tree() -> void:
	if is_instance_valid(graph_view):
		remove_control_from_bottom_panel(graph_view)
		graph_view.queue_free()


func _has_main_screen() -> bool:
	return false

#
#func _make_visible(next_visible: bool) -> void:
	#if is_instance_valid(main_view):
		#main_view.visible = next_visible
		#if next_visible:
			#main_view.setup(self, get_undo_redo())


func _get_plugin_name() -> String:
	return "Issue"
#
#func _apply_changes() -> void:
	#if is_instance_valid(main_view):
		#main_view.apply_changes()


# not used when the plugin is in the bottom panel area. Ah well.
#func _get_plugin_icon() -> Texture2D:
#	return create_main_icon()


#func create_main_icon(scale: float = 1.0) -> Texture2D:
#	var size: Vector2 = Vector2(16, 16) * get_editor_interface().get_editor_scale() * scale
#	#var base_color: Color = get_editor_interface().get_editor_main_screen().get_theme_color("base_color", "Editor")
#	#var theme: String = "light" if base_color.v > 0.5 else "dark"
#	var base_icon = load("res://addons/resource_wrangler/assets/resource_wrangler_icon_large_cleaned.svg") as Texture2D
#	var image: Image = base_icon.get_image()
#	image.resize(size.x, size.y, Image.INTERPOLATE_TRILINEAR)
#	return ImageTexture.create_from_image(image)

